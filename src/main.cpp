
#include <jack/jack.h>
#include "JACK.h"
#include "LoopbackCapture.h"
#include "common.h"
#include "JackClient.h"
#include "Queue.h"

void usage(char* name) {
    fprintf(stderr, "Usage: %s <PID>\n", name);
}

int main(int argc, char* argv[]) {
    if(argc < 2) {
        usage(argv[0]);
        return 1;
    }

    DWORD processID = strtoul(argv[1], nullptr, 0);
    if(processID == 0) {
        usage(argv[0]);
        return 1;
    }

    Queue<Frame> frameQueue;
    JackClient jackClient(&frameQueue);
    LoopbackCapture loopbackCapture(&frameQueue);
    HRESULT hr = loopbackCapture.StartCaptureAsync(processID);
    if(FAILED(hr)) {
        wil::unique_hlocal_string message;
        FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER, nullptr, hr,
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (PWSTR)&message, 0, nullptr);
        std::wcout << L"Failed to start capture\n0x" << std::hex << hr << L": " << message.get() << L"\n";
        return 1;
    }
    // Sleep for 30 seconds
    Sleep(30000);
    loopbackCapture.StopCaptureAsync();
    std::wcout << "Finished.\n";
    return 0;
}