#ifndef __ASYNC_CALLBACK_H__
#define __ASYNC_CALLBACK_H__
#include <mfobjects.h>
#include <functional>
#include <wrl\implements.h>
#include <wil\com.h>
#include <wil\result.h>
#include <iostream>


template<class T>
class AsyncCallback :
    public IMFAsyncCallback
{
protected:
    T* parent;
    DWORD dwQueueID;
    std::function<HRESULT(IMFAsyncResult*)> callback;
public:
    AsyncCallback(
        T* parent,
        std::function<HRESULT(IMFAsyncResult*)> callback
    ) :
        parent(parent), callback(callback),
        dwQueueID(MFASYNC_CALLBACK_QUEUE_MULTITHREADED) { }

    STDMETHOD_(ULONG, AddRef)() {
        return parent->AddRef();
    }

    STDMETHOD_(ULONG, Release)() {
        return parent->Release();
    }

    STDMETHOD(QueryInterface)(REFIID riid, void** ppvObject) {
        if (riid == IID_IMFAsyncCallback || riid == IID_IUnknown) {
            *ppvObject = this;
            AddRef();
            return S_OK;
        }
        *ppvObject = NULL;
        return E_NOINTERFACE;
    }
    STDMETHOD(GetParameters)(
        __RPC__out DWORD* pdwFlags,
        __RPC__out DWORD* pdwQueue
    ) {
        *pdwFlags = 0;
        *pdwQueue = dwQueueID;
        return S_OK;
    }

    STDMETHOD(Invoke)(__RPC__out IMFAsyncResult* pResult) {
        callback(pResult);
        return S_OK;
    }

    void SetQueueID(DWORD dwQueueID) {
        this->dwQueueID = dwQueueID;
    }
    
};
#endif//__ASYNC_CALLBACK_H__