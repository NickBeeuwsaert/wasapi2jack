#ifndef __JACK_CLIENT_H__
#define __JACK_CLIENT_H__
#include <queue>
#include <mutex>
#include "common.h"
#include "JackClient.h"
#include "JACK.h"
#include "Queue.h"

class JackClient {
    JACK jack;
    Queue<Frame>* frameQueue;
    jack_port_t *left_port, *right_port;
    jack_client_t* client;
public:
    JackClient(Queue<Frame>*);
    ~JackClient();
    int process(jack_nframes_t);
};

#endif//__JACK_CLIENT_H__