#include <shlobj.h>
#include <wchar.h>
#include <iostream> // ?
#include <audioclientactivationparams.h>
#include <mfapi.h>

#include <functional>
#include <mutex>
#include <queue>

#include "AsyncCallback.h"
#include "LoopbackCapture.h"

using namespace std::placeholders;

LoopbackCapture::LoopbackCapture(Queue<Frame>* frameQueue) :
    frameQueue(frameQueue),
    StartCapture (this, std::bind(&LoopbackCapture::OnStartCapture,  this, _1)),
    StopCapture  (this, std::bind(&LoopbackCapture::OnStopCapture,   this, _1)),
    SampleReady  (this, std::bind(&LoopbackCapture::OnSampleReady,   this, _1)),
    FinishCapture(this, std::bind(&LoopbackCapture::OnFinishCapture, this, _1)) {}

LoopbackCapture::~LoopbackCapture() {
    if(queueID != 0) {
        MFUnlockWorkQueue(queueID);
    }
}

HRESULT LoopbackCapture::SetDeviceStateErrorIfFailed(HRESULT hr) {
    if(FAILED(hr)) {
        deviceState = DeviceState::Error;
    }
    return hr;
}

HRESULT LoopbackCapture::InitializeLoopbackCapture() {
    RETURN_IF_FAILED(sampleReadyEvent.create(wil::EventOptions::None));

    RETURN_IF_FAILED(MFStartup(MF_VERSION, MFSTARTUP_LITE));

    DWORD taskID = 0;
    RETURN_IF_FAILED(MFLockSharedWorkQueue(L"Capture", 0, &taskID, &queueID));

    SampleReady.SetQueueID(queueID);

    RETURN_IF_FAILED(activateCompleted.create(wil::EventOptions::None));
    RETURN_IF_FAILED(   captureStopped.create(wil::EventOptions::None));

    return S_OK;
}

HRESULT LoopbackCapture::ActivateAudioInterface(DWORD processId) {
    return SetDeviceStateErrorIfFailed([&]() -> HRESULT {
        AUDIOCLIENT_ACTIVATION_PARAMS audioClientParams = {};
        audioClientParams.ActivationType = AUDIOCLIENT_ACTIVATION_TYPE_PROCESS_LOOPBACK;
        audioClientParams.ProcessLoopbackParams.ProcessLoopbackMode = PROCESS_LOOPBACK_MODE_INCLUDE_TARGET_PROCESS_TREE;
        audioClientParams.ProcessLoopbackParams.TargetProcessId = processId;

        PROPVARIANT activateParams = {};
        activateParams.vt = VT_BLOB;
        activateParams.blob.cbSize = sizeof(audioClientParams);
        activateParams.blob.pBlobData = (BYTE*)&audioClientParams;

        wil::com_ptr_nothrow<IActivateAudioInterfaceAsyncOperation> asyncOp;
        RETURN_IF_FAILED(ActivateAudioInterfaceAsync(VIRTUAL_AUDIO_DEVICE_PROCESS_LOOPBACK, __uuidof(IAudioClient), &activateParams, this, &asyncOp));
        activateCompleted.wait();

        return activateResult;
    }());
}

HRESULT LoopbackCapture::ActivateCompleted(IActivateAudioInterfaceAsyncOperation* operation) {
    activateResult = SetDeviceStateErrorIfFailed([&]() -> HRESULT {
        HRESULT activateResult = E_UNEXPECTED;
        wil::com_ptr_nothrow<IUnknown> punkAudioInterface;
        RETURN_IF_FAILED(operation->GetActivateResult(&activateResult, &punkAudioInterface));
        RETURN_IF_FAILED(activateResult);

        RETURN_IF_FAILED(punkAudioInterface.copy_to(&audioClient));

        captureFormat.wFormatTag = WAVE_FORMAT_PCM;
        captureFormat.nChannels = 2;
        captureFormat.nSamplesPerSec = 44100;
        captureFormat.wBitsPerSample = 16; // each sample is 2 bytes
        captureFormat.nBlockAlign = captureFormat.nChannels * captureFormat.wBitsPerSample / 8;
        captureFormat.nAvgBytesPerSec = captureFormat.nSamplesPerSec * captureFormat.nBlockAlign;

        RETURN_IF_FAILED(
            audioClient->Initialize(
                AUDCLNT_SHAREMODE_SHARED,
                AUDCLNT_STREAMFLAGS_LOOPBACK | AUDCLNT_STREAMFLAGS_EVENTCALLBACK,
                200000 * 10,
                AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM,
                &captureFormat,
                nullptr
            )
        );

        RETURN_IF_FAILED(audioClient->GetBufferSize(&bufferFrames));
        RETURN_IF_FAILED(audioClient->GetService(IID_PPV_ARGS(&audioCaptureClient)));
        RETURN_IF_FAILED(MFCreateAsyncResult(nullptr, &SampleReady, nullptr, &sampleReadyAsyncResult));
        RETURN_IF_FAILED(audioClient->SetEventHandle(sampleReadyEvent.get()));

        deviceState = DeviceState::Initialized;
        return S_OK;
    }());
    activateCompleted.SetEvent();
    return S_OK;
}

HRESULT LoopbackCapture::StartCaptureAsync(DWORD processId) {
    RETURN_IF_FAILED(InitializeLoopbackCapture());

    RETURN_IF_FAILED(ActivateAudioInterface(processId));

    if(deviceState == DeviceState::Initialized) {
        deviceState = DeviceState::Starting;

        return MFPutWorkItem2(MFASYNC_CALLBACK_QUEUE_MULTITHREADED, 0, &StartCapture, nullptr);
    }
    return S_OK;
}
HRESULT LoopbackCapture::StopCaptureAsync() {
    RETURN_HR_IF(E_NOT_VALID_STATE, deviceState != DeviceState::Capturing);
    deviceState = DeviceState::Stopping;

    RETURN_IF_FAILED(MFPutWorkItem2(MFASYNC_CALLBACK_QUEUE_MULTITHREADED, 0, &StopCapture, nullptr));

    captureStopped.wait();

    return S_OK;
}
HRESULT LoopbackCapture::FinishCaptureAsync() {
    return MFPutWorkItem2(MFASYNC_CALLBACK_QUEUE_MULTITHREADED, 0, &FinishCapture, nullptr);
}
HRESULT LoopbackCapture::OnAudioSampleRequested() {
    count += 1;
    UINT32 framesAvailable = 0;
    BYTE* data = nullptr;
    DWORD captureFlags;
    UINT64 devicePosition = 0;
    UINT64 QPCPosition = 0;
    DWORD bytesToCapture = 0;
    auto lock = critSec.lock();

    if(deviceState == DeviceState::Stopping) return S_OK;
    // mutex->lock();
    while(SUCCEEDED(audioCaptureClient->GetNextPacketSize(&framesAvailable)) && framesAvailable > 0) {
        
        bytesToCapture = framesAvailable * captureFormat.nBlockAlign;

        RETURN_IF_FAILED(
            audioCaptureClient->GetBuffer(&data, &framesAvailable, &captureFlags, &devicePosition, &QPCPosition)
        );

        if(deviceState == DeviceState::Stopping) return S_OK;

        int16_t* frames = (int16_t*)data;

        // 2 channels, stepping by 2 for each short
        for(int i = 0; i < framesAvailable; i ++) {
            int16_t left = frames[i*2];
            int16_t right = frames[i*2 + 1];
            frameQueue->push(Frame { left / 32768.f, right / 32768.f});
        }
        // Not needed, reference only has this for WAV file limit
        // if((dataSize + bytesToCapture) < dataSize) {
        //     StopCaptureAsync();
        //     break;
        // }
        // Write data to JACK queue
        // someQueue.push(data...)

        audioCaptureClient->ReleaseBuffer(framesAvailable);
    }

    // mutex->unlock();

    return S_OK;
}


HRESULT LoopbackCapture::OnStartCapture(IMFAsyncResult* result) {
    return SetDeviceStateErrorIfFailed([&]() -> HRESULT {
        RETURN_IF_FAILED(audioClient->Start());
        deviceState = DeviceState::Capturing;
        MFPutWaitingWorkItem(sampleReadyEvent.get(), 0, sampleReadyAsyncResult.get(), &sampleReadyKey);

        return S_OK;
    }());
}
HRESULT LoopbackCapture::OnStopCapture(IMFAsyncResult* result) {
    if(sampleReadyKey != 0) {
        MFCancelWorkItem(sampleReadyKey);
        sampleReadyKey = 0;
    }

    audioClient->Stop();
    sampleReadyAsyncResult.reset();

    return FinishCaptureAsync();
}
HRESULT LoopbackCapture::OnFinishCapture(IMFAsyncResult* result) {
    // TODO: Signal to stop jack client here
    deviceState = DeviceState::Stopped;
    captureStopped.SetEvent();
    return S_OK;
}
HRESULT LoopbackCapture::OnSampleReady(IMFAsyncResult* result) {
    if(SUCCEEDED(OnAudioSampleRequested())) {
        if(deviceState != DeviceState::Capturing) return S_OK;
        return MFPutWaitingWorkItem(sampleReadyEvent.get(), 0, sampleReadyAsyncResult.get(), &sampleReadyKey);
    } else {
        deviceState = DeviceState::Error;
    }
    return S_OK;
}

