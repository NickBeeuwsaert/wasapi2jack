set(CMAKE_VS_WINDOWS_TARGET_PLATFORM_VERSION 10.0.20348.0)
find_package(wil)

add_executable(JackTest main.cpp LoopbackCapture.cpp JackClient.cpp)
target_link_libraries(JackTest
    PRIVATE
    dxva2.lib evr.lib
    mf.lib mfplat.lib mfplay.lib mfreadwrite.lib mfuuid.lib
    mmdevapi.lib
)   