#ifndef __JACK_WRAPPER_H__
#define __JACK_WRAPPER_H__
#include <windows.h>
#include <jack/jack.h>
#include <iostream>

class SharedProcedure {
    FARPROC method;

public:
    SharedProcedure(FARPROC procedure) : method(procedure) {}

    template<typename T>
    operator T*() const {
        return reinterpret_cast<T*>(method);
    }
};

class SharedLibrary {
    HMODULE library;
public:
    SharedLibrary(LPCTSTR library) : library(LoadLibrary(library)) {

    }
    ~SharedLibrary() {
        FreeLibrary(library);
    }

    SharedProcedure operator[](LPCTSTR method) {
        return SharedProcedure(GetProcAddress(library, method));
    }
};

class JACK {
    SharedLibrary jack{"libjack64.dll"};
public:
    decltype(jack_client_open)* client_open = jack["jack_client_open"];
    decltype(jack_client_close)* client_close = jack["jack_client_close"];
    decltype(jack_get_client_name)* get_client_name = jack["jack_get_client_name"];
    decltype(jack_set_process_callback)* set_process_callback = jack["jack_set_process_callback"];
    decltype(jack_on_shutdown)* on_shutdown = jack["jack_on_shutdown"];
    decltype(jack_activate)* activate = jack["jack_activate"];
    decltype(jack_port_register)* port_register = jack["jack_port_register"];
    decltype(jack_port_get_buffer)* port_get_buffer = jack["jack_port_get_buffer"];
};

#endif // __JACK_WRAPPER_H__