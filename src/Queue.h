#ifndef __QUEUE_H__
#define __QUEUE_H__
#include <queue>
#include <mutex>
#include <condition_variable>

template<typename T>
class Queue {
    std::queue<T> queue;
    std::mutex mutex;
    std::condition_variable condition;

public:
    T pop() {
        T value;
        {
            std::unique_lock<std::mutex> guard(mutex);
            while(queue.empty()) condition.wait(guard);
            value = queue.front();
            queue.pop();
        }
        condition.notify_all();
        return value;
    }

    void push(T value) {
        {
            std::lock_guard<std::mutex> guard(mutex);
            queue.push(value);
        }
        condition.notify_all();
    }
};
#endif//__QUEUE_H__