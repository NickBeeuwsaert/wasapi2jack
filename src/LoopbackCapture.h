#ifndef __LOOPBACK_CAPTURE__
#define __LOOPBACK_CAPTURE__
#include <AudioClient.h>
#include <mmdeviceapi.h>
#include <initguid.h>
#include <guiddef.h>
#include <mfapi.h>

#include <wrl/implements.h>
#include <wil/com.h>
#include <wil/result.h>

#include <queue>
#include <mutex>

#include "AsyncCallback.h"
#include "common.h"
#include "Queue.h"

using namespace Microsoft::WRL;

enum class DeviceState {
    Uninitialized,
    Error,
    Initialized,
    Starting,
    Capturing,
    Stopping,
    Stopped
};

class LoopbackCapture :
    public RuntimeClass<
        RuntimeClassFlags<ClassicCom>,
        FtmBase,
        IActivateAudioInterfaceCompletionHandler
    >
{

    Queue<Frame>* frameQueue;
    // std::mutex* mutex;

    HRESULT OnStartCapture(IMFAsyncResult*);
    HRESULT OnStopCapture(IMFAsyncResult*);
    HRESULT OnFinishCapture(IMFAsyncResult*);
    HRESULT OnSampleReady(IMFAsyncResult*);

    HRESULT InitializeLoopbackCapture();
    HRESULT OnAudioSampleRequested();
    HRESULT ActivateAudioInterface(DWORD);
    HRESULT FinishCaptureAsync();

    HRESULT SetDeviceStateErrorIfFailed(HRESULT);

    wil::com_ptr_nothrow<IAudioClient> audioClient;
    WAVEFORMATEX captureFormat{};
    UINT32 bufferFrames;
    wil::com_ptr_nothrow<IAudioCaptureClient> audioCaptureClient;
    wil::com_ptr_nothrow<IMFAsyncResult> sampleReadyAsyncResult;

    wil::unique_event_nothrow sampleReadyEvent;
    MFWORKITEM_KEY sampleReadyKey;
    wil::critical_section critSec; // Needed?

    DWORD queueID = 0;
    DWORD cbHeaderSize = 0; // Needed?
    DWORD dataSize = 0;

    HRESULT activateResult = E_UNEXPECTED;

    DeviceState deviceState { DeviceState::Uninitialized };
    wil::unique_event_nothrow activateCompleted;
    wil::unique_event_nothrow captureStopped;
public:
    LoopbackCapture(Queue<Frame>*);
    ~LoopbackCapture();

    HRESULT StartCaptureAsync(DWORD);
    HRESULT StopCaptureAsync();

    AsyncCallback<LoopbackCapture> StartCapture;
    AsyncCallback<LoopbackCapture> StopCapture;
    AsyncCallback<LoopbackCapture> SampleReady;
    AsyncCallback<LoopbackCapture> FinishCapture;

    // IActivateAudioInterfaceCompletionHandler
    STDMETHOD(ActivateCompleted)(IActivateAudioInterfaceAsyncOperation*);

    int count = 0;


};

#endif//__LOOPBACK_CAPTURE__