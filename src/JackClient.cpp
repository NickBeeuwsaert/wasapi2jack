#include <jack/jack.h>
#include <mutex>
#include <queue>
#include "JackClient.h"
#include "Queue.h"

int _process (jack_nframes_t nframes, void *arg) {
    return ((JackClient*)arg)->process(nframes);
}

JackClient::JackClient(Queue<Frame>* frameQueue) : frameQueue(frameQueue) {
    // Probably not a good idea to do this initialization in the constructor
    jack_status_t status;
    client = jack.client_open("jack test", JackNullOption, &status, NULL);

    if(client == NULL) {
        printf("Yo, couldn't connect to JACK2\n");
        exit(1);
    }

    if(status & JackServerStarted) {
        printf("Started JACK2 server\n");
    }

    if(status & JackNameNotUnique) {
        printf("Yo, client name wasn't unique.");
    }

    jack.set_process_callback(client, &_process, this);
    left_port  = jack.port_register(client, "output 1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    right_port = jack.port_register(client, "output 2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

    if(left_port == NULL || right_port == NULL) {
        printf("No more jack ports available\n");
        exit(1);
    }

    jack.activate(client);
}

JackClient::~JackClient() {
    jack.client_close(client);
}

int JackClient::process(jack_nframes_t nframes) {
    jack_default_audio_sample_t *left_out, *right_out;
    left_out  = (jack_default_audio_sample_t*)jack.port_get_buffer(left_port,  nframes);
    right_out = (jack_default_audio_sample_t*)jack.port_get_buffer(right_port, nframes);
    int i = 0;
    for(i = 0; i < nframes; i++) {
        Frame frame = frameQueue->pop();
        left_out[i] = frame.left;
        right_out[i] = frame.right;
    }
    return 0;
}

