# JackBridge
Forward an applications audio onto JACK in Windows 10

## Setup
```
vcpkg integrate install
vcpkg install wil:x64-windows
vcpkg install jack2:x64-windows

cmake -B build -S . -DCMAKE_TOOLCHAIN_FILE=[path to vcpkg]/scripts/buildsystems/vcpkg.cmake
cmake --build build
# exe is in build/src/Debug
```

## Usage
```
JackTest.exe <PID> # Capture 30s of audio (for now)
```